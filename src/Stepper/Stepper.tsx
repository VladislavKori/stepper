import React, { useEffect } from 'react'
import './Stepper.scss'

import { steps } from '../fake/steps';

interface Step {
    id: number
    status: string
    inProcess: boolean
    done: boolean
}

function Stepper() {

    // main logic
    let maxPointsInView = 6;
    const processingSteps = [] as Step[]
    let processId = steps.find(item => item.inProcess === true)?.id || null
   
    if (processId == null) { processId = 0 };
    const processPosition = processId % maxPointsInView;
    const activeBlock = Math.floor(processId / maxPointsInView);

    const stepsMoreThanMaxPoints = steps.length > maxPointsInView * activeBlock + maxPointsInView;

    // logic for process
    const startViewWithPoint = (function () {
        const remained = steps.length - (activeBlock * maxPointsInView);
        if (remained <= maxPointsInView) {
            return steps.length - maxPointsInView;
        }

        return activeBlock * maxPointsInView;
    })();

    const endViewWithPoint = (function () {
        const remained = steps.length - (activeBlock * maxPointsInView);
        if (remained <= maxPointsInView) {
            return steps.length;
        }
        return activeBlock * maxPointsInView + maxPointsInView;
    })();
    
    processingSteps.push(...steps.slice(startViewWithPoint, endViewWithPoint));

    // loginc for last point
    const lastStepIsDone = steps[steps.length - 1].done;

    return (
        <div className="stepper">
            {processingSteps.map((item, index) => {

                const currentStepNumber = item.id + 1;
                let resultClassName = ""
                if (item.inProcess) { resultClassName = "stepper__item_inprocess" }
                if (item.done) { resultClassName = "stepper__item_done" }

                return (
                    <div className={"stepper__item" + " " + resultClassName}>
                        <span className="stepper__point"></span>
                        <h2 className="stepper__number">{currentStepNumber}</h2>
                        <p className="stepper__status">{item.status}</p>
                    </div>
                )
            })}

            <div className={lastStepIsDone ? "stepper__item_end stepper__item_done" : "stepper__item_end"}>
                <span className="stepper__point"></span>
                <h2 className="stepper__number">
                    {stepsMoreThanMaxPoints ? "..." : steps.length + 1}
                </h2>
            </div>

        </div>
    )
}

export default Stepper