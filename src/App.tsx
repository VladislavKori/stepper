import React from 'react'
import './App.scss'
import Stepper from './Stepper/Stepper'

function App() {

  return (
    <>
      <div className="container">
        <Stepper />
      </div>
    </>
  )
}

export default App
