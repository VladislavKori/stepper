interface Step {
    id: number
    status: string
    inProcess: boolean
    done: boolean
}

export const steps = [
    {
        id: 0,
        status: "Завершено",
        inProcess: false,
        done: true
    },
    {
        id: 1,
        status: "В процессе",
        inProcess: true,
        done: false
    },
    {
        id: 2,
        status: "Не активно",
        inProcess: false,
        done: false
    },
    {
        id: 3,
        status: "Не активно",
        inProcess: false,
        done: false
    },
    {
        id: 4,
        status: "Не активно",
        inProcess: false,
        done: false
    },
    {
        id: 5,
        status: "Не активно",
        inProcess: false,
        done: false
    },
    {
        id: 6,
        status: "Не активно",
        inProcess: false,
        done: false
    },
] as Step[];
